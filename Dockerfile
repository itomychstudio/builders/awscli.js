FROM node:14-alpine3.14

RUN apk add --no-cache \
        git \
        python3 \
        py3-pip \
        make \
        g++ \
    && pip3 install --no-cache-dir --upgrade \
        awscli \
    && rm -rf \
        /tmp/* \
        /var/cache/apk/* \
        /var/tmp/*
